#!/usr/bin/env node
/*FILE_BEG {"actions": ["jslint", "cover", "eval"], "name": "/FS_MODULE/moduleInit.js"}*/
/*jslint bitwise: true, evil: true, indent: 2, nomen: true, regexp: true, stupid: true*/

/**
 * @class EXPORTS
 */

/**
 * initialization code
 * @module moduleInit
 */

(function () {

  'use strict';

  var EXPORTS = global.EXPORTS = global.EXPORTS || {}, local = {

    _init: function () {
      //// debug
      global[String('p' + 'rintd')] = console.log;
      //// EXPORTS
      EXPORTS.exportLocal = local.exportLocal;
      EXPORTS.exportLocal(local);
      EXPORTS.require = EXPORTS.require || require;
      EXPORTS.requireChildProcess = EXPORTS.requireChildProcess || require('child_process');
      EXPORTS.requireCommander = EXPORTS.requireCommander || require('commander');
      EXPORTS.requireCrypto = EXPORTS.requireCrypto || require('crypto');
      EXPORTS.requireExpress = EXPORTS.requireExpress || require('express');
      EXPORTS.requireFs = EXPORTS.requireFs || require('fs');
      EXPORTS.requireFsExtra = EXPORTS.requireFsExtra || require('fs-extra');
      EXPORTS.requireHttp = EXPORTS.requireHttp || require('http');
      EXPORTS.requireIstanbul = EXPORTS.requireIstanbul || require('istanbul');
      EXPORTS.requireJslintLinter = EXPORTS.requireJslintLinter || require('jslint/lib/linter');
      EXPORTS.requireJslintReporter = EXPORTS.requireJslintReporter || require('jslint/lib/reporter');
      EXPORTS.requireMime = EXPORTS.requireMime || require('mime');
      EXPORTS.requireModule = EXPORTS.requireModule || require('module');
      EXPORTS.requirePath = EXPORTS.requirePath || require('path');
      EXPORTS.requireRepl = EXPORTS.requireRepl || require('repl');
      EXPORTS.requireUrl = EXPORTS.requireUrl || require('url');
      EXPORTS.requireUtil = EXPORTS.requireUtil || require('util');
      EXPORTS.requireVm = EXPORTS.requireVm || require('vm');
      EXPORTS.COVER = EXPORTS.COVER || new EXPORTS.requireIstanbul.Instrumenter();
      EXPORTS.COVER_FILE = EXPORTS.COVER_FILE || {};
      EXPORTS.FS_CWD = EXPORTS.FS_CWD || process.cwd();
      EXPORTS.FS_MODULE = EXPORTS.FS_MODULE || EXPORTS.requirePath.dirname(module.filename);
      EXPORTS.FS_MODULE_EXTERNAL = EXPORTS.FS_MODULE_EXTERNAL || EXPORTS.requirePath.dirname(require.resolve('tinyjs-external'));
      EXPORTS.IS_CLI = EXPORTS.IS_CLI || module === require.main;
      EXPORTS.IS_NODEJS = EXPORTS.IS_NODEJS || true;
      EXPORTS.JSLINT_WATCH = EXPORTS.JSLINT_WATCH || {};
      EXPORTS.MODULE = EXPORTS.MODULE || module;
      EXPORTS.MODULES = EXPORTS.MODULES || {};
      EXPORTS.MODULE_ACTION = {
        base64Decode: EXPORTS.moduleBase64Decode,
        cover: EXPORTS.moduleCover,
        eval: EXPORTS.moduleEval,
        jslint: EXPORTS.moduleJslint,
        write: EXPORTS.moduleWrite
      };
      /**
       * @attribute MODULE_FILE
       */
      EXPORTS.MODULE_FILE = EXPORTS.MODULE_FILE || {};
      EXPORTS.MODULE_WATCH = EXPORTS.MODULE_WATCH || {};
      //// reload self
      if (!EXPORTS.MODULE_FILE.hasOwnProperty(module.filename)) {
        EXPORTS.moduleLoad(module);
        return;
      }
      EXPORTS.requireIstanbul.Store.mix(EXPORTS.requireIstanbul.Report.create('html').opts.sourceStore.constructor, {
        get: function (key) {
          return EXPORTS.COVER_FILE[key];
        }
      });
      EXPORTS.STR_ASCII = EXPORTS.STR_ASCII || '\u0000\u0001\u0002\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
      EXPORTS.TEST = EXPORTS.TEST || {};
      EXPORTS.TEST_HTTP = EXPORTS.TEST_HTTP || {};
      //// initialize command-line code
      if (EXPORTS.IS_CLI) {
        EXPORTS.ARGV = EXPORTS.ARGV || EXPORTS.requireCommander
          .option('--build', 'build mode')
          .option('--cover', 'enable code coverage')
          .option('--debug', 'debug mode')
          .option('--port []', 'http port', parseInt)
          .parse(process.argv);
        Object.keys(EXPORTS.ARGV).forEach(function (key) {
          var val = EXPORTS.ARGV[key];
          if (!val) { return; }
          //// auto-set environment variables
          try { val = JSON.parse(val); } catch (errParse) {}
          EXPORTS['IS_' + key.replace((/\W/g), '_').toUpperCase()] = val;
          switch (key) {
          case 'port':
            EXPORTS.HTTP_PORT = val;
            break;
          }
        });
        //// initialize command-line modules
        EXPORTS.ARGV.args.forEach(function (filename) {
          EXPORTS.moduleLoad(EXPORTS.requirePath.resolve(EXPORTS.FS_CWD, filename));
        });
        if (process.EXIT) { return; }
      } else {
        //// display environment
        console.log(['process.argv', process.argv]);
        console.log(['cwd', process.cwd()]);
        console.log(['nodejs', process.version]);
        console.log(['module', module.id, module.filename]);
      }
      //// require
      require.extensions['.js2'] = EXPORTS.moduleLoad;
      require('./moduleMain.js2');
    },

    coverClear: function () {
      //// clear __coverage__
      var arr, cov = global.__coverage__, key, obj;
      for (key in cov) {
        if (cov.hasOwnProperty(key)) {
          obj = cov[key];
          arr = obj.b;
          for (key in arr) { if (arr.hasOwnProperty(key)) { arr[key] = [0, 0]; } }
          arr = obj.f;
          for (key in arr) { if (arr.hasOwnProperty(key)) { arr[key] = 0; } }
          arr = obj.s;
          for (key in arr) { if (arr.hasOwnProperty(key)) { arr[key] = 0; } }
        }
      }
    },

    coverInstrument: function (code, filename) {
      var ii = 0;
      switch ((/\w*$/).exec(filename)[0]) {
      //// instrument javascript embedded in .html file
      case 'html':
        EXPORTS.COVER_FILE[filename] = code;
        return code.replace((/<script>([\S\s]+)<\/script>/g), function (_, code) {
          return '<script>' + EXPORTS.COVER.instrumentSync(code, filename + '.script.' + (ii += 1)) + '</script>';
        });
      //// instrument .js file
      case 'js':
        EXPORTS.COVER_FILE[filename] = code;
        return EXPORTS.COVER.instrumentSync(code, filename);
      default:
        return code;
      }
    },

    coverReport: function () {
      var collector = new EXPORTS.requireIstanbul.Collector();
      collector.add(global.__coverage__);
      EXPORTS.requireIstanbul.Report.create('lcov', { dir: EXPORTS.FS_CWD + '/coverage' }).writeReport(collector);
    },

    exportLocal: function (local) {
      //// export local methods to EXPORTS
      var arr, ii, key;
      for (key in local) {
        if (local.hasOwnProperty(key) && key[0] !== '_') {
          EXPORTS[key] = local[key];
        }
      }
    },

    jslintReport: function (filename, code) {
      code = code || EXPORTS.requireFs.readFileSync(filename, 'utf8');
      //// jslint ignore
      code = code.replace(
        (/^\/\*JSLINT_IGNORE_BEG\*\/$[\S\s]*?^\/\*JSLINT_IGNORE_END\*\/$/gm),
        local._jslintReportIgnore
      );
      switch (EXPORTS.requirePath.extname(filename)) {
      case '.css':
        code = code.replace((/^(\n+)@charset "UTF-8";/), '@charset "UTF-8";$1');
        break;
      }
      //// jslint file and report to stdout
      EXPORTS.requireJslintReporter.report(
        filename,
        EXPORTS.requireJslintLinter.lint(code),
        true
      );
    },

    _jslintReportIgnore: function (code) {
      //// OPTIMIZATION - cached callback
      return code.match('\n').join('');
    },

    /**
     * watch file and auto-jslint if modified
     * @method jslintWatch
     * @param {String} filename
     * @static
     */
    jslintWatch: function (filename) {
      EXPORTS.JSLINT_WATCH[filename] = EXPORTS.JSLINT_WATCH[filename] || EXPORTS.requireFs.watchFile(
        filename,
        {interval: 1000, persistent: false},
        function (curr, prev) {
          if (curr.mtime > prev.mtime) { EXPORTS.jslintReport(filename); }
        }
      );
    },

    moduleBase64Decode: function (module, file, code) {
      EXPORTS.MODULE_FILE[file.name] = new Buffer(code, 'base64');
    },

    moduleCover: function (module, file, code) {
      if (EXPORTS.IS_COVER) {
        EXPORTS.MODULE_FILE[file.name] = EXPORTS.coverInstrument(code, module.filename + '.' + file.name);
      }
    },

    moduleEval: function (module, file, code) {
      code = code.replace((/^#!/m), '// #!');
      module._compile(code, module.filename);
    },

    moduleJslint: function (module, file, code) {
      code = code.replace((/^#!/m), '// #!');
      EXPORTS.jslintReport(file.name, code);
    },

    moduleLoad: function (module) {
      //// custom module loader
      var action, content = EXPORTS.requireFs.readFileSync(module.filename || module, 'utf8'), ii;
      //// if module is a filename, create a new module with filename as module id
      if (typeof module === 'string') {
        module = EXPORTS.requirePath.resolve(EXPORTS.FS_CWD, module);
        module = EXPORTS.MODULES[module] || new EXPORTS.requireModule.Module(module);
        module.filename = module.filename || module.id;
      }
      EXPORTS.MODULES[module.filename] = module;
      //// bump up version
      EXPORTS.VERSION = new Date().toISOString().replace((/(....)-(..)-(..)T(..):(..):(..).*/), '$1.$2.$3-$4.$5.$6');
      return content.replace((/^\/\*FILE_BEG (.*?)\*\/$([\S\s]*?)^\/\*FILE_END\*\/$/gm), function (_, file, code, start) {
        file = JSON.parse(file);
        //// filename
        file.name = file.name
          .replace((/\/FS_MODULE_EXTERNAL\b/), EXPORTS.FS_MODULE_EXTERNAL)
          .replace((/\/FS_MODULE\b/), EXPORTS.FS_MODULE);
        //// preserve line number position
        code = (content.slice(0, start).match(/\n/g) || []).join('') + code;
        //// save source code
        EXPORTS.MODULE_FILE[file.name] = EXPORTS.stringFormat ? EXPORTS.stringFormat(code, { EXPORTS: EXPORTS }) : code;
        for (ii = 0; ii < file.actions.length; ii += 1) {
          action = file.actions[ii];
          EXPORTS.MODULE_ACTION[action](module, file, EXPORTS.MODULE_FILE[file.name]);
        }
        if (ii) {
          //// auto-reload module if modified
          EXPORTS.MODULE_WATCH[module.filename] = EXPORTS.MODULE_WATCH[module.filename] || EXPORTS.requireFs.watchFile(module.filename, {interval: 1000, persistent: false}, function (curr, prev) {
            //// reload module if modified
            if (curr.mtime > prev.mtime) { EXPORTS.moduleLoad(module, null, 'reload'); }
          });
        } else {
          module._compile(content, module.id);
        }
      });
    },

    moduleWrite: function (module, file, code) {
      //// write code to file
      if (EXPORTS.IS_BUILD) {
        EXPORTS.requireFs.writeFileSync(
          EXPORTS.requirePath.resolve(EXPORTS.FS_CWD, file.name),
          //// trim text
          typeof code === 'string' ? code.trim() : code
        );
      }
    }

  };

  local._init();

}());
/*FILE_END*/
